TEST_CMD = @./node_modules/.bin/mocha --compilers coffee:coffee-script spec/*_spec.coffee

build:
	@./node_modules/.bin/coffee --output lib/ --bare --compile src/

test: build
	$(TEST_CMD) --reporter spec

test-watch: build
	$(TEST_CMD) --reporter min --watch --growl

deploy:
	@git push

.PHONY: build