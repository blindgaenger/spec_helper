spec_helper
===========

your specs need help

Require it in your specs … best with coffee and mocha.

    {sinon, expect, proxyquire, sandbox, fakeweb, debug, fixture} = require 'spec_helper'

    describe '#toUpperCase', ->
      it 'works', ->
        expect('foo'.toUpperCase()).to.eql('FOO')


## sandbox

Create a sinon sandbox without worries. If you've ever forgotten to verify your
sinon mocks, or some stubs weren't restored and caused side effects in other tests,
than you'll like this.

    alice = new Person()

    sandbox (box) =>
      aliceMock = box.mock(alice)
      aliceMock.expects('greet')

      alice.greet()

      # that's it, nothing more to do!

Note, that `box` is just a sinon sandbox, which scopes all subs created inside.
Mostly I like to name the `box` variable `sinon`, to easily


## fakeweb

Mock web requests with an easy to remember syntax. Internally it uses nock, but only simple methods
are exposed. For me this solves more than 90% of my http problems. So if you really need advanced
mocking, use nock by yourself.

Requests:

    fakeweb (web) =>
      web.get <URL>, <STATUS>, <BODY>

Redirects:

    fakeweb (web) =>
      web.redirect <SOURCE_URL>, <TARGET_URL>

## debug

Nothing special, just an helper to print `util.inspect`. But very useful if you try to debug your test.

    person =
      name: 'Alice'

    debug person

    # prints something like { name: 'Alice' }


## fixture

TODO

## TODOS

- debug(arguments)
- diff(obj)
- fakeweb#request
- fixtures __dirname
- fix path for spec_helper#proxyquire
- proxyquire.noCallThru().load('../lib/http', {})
- rename to spec-helper
