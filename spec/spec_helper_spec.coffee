specHelper = require '../lib/spec_helper'
assert = require 'assert'
http = require 'http'
url = require 'url'

describe 'specHelper', ->

  describe 'module', ->
    it 'exports sinon', ->
      assert.equal specHelper.sinon, require('sinon')

    it 'exports chai expect', ->
      assert.equal specHelper.expect, require('chai').expect

    it 'exports proxyquire', ->
      try
        specHelper.proxyquire(null)
      catch e
        assert.equal e.name, 'ProxyquireError'

  describe '#sandbox', ->
    it 'creates sinon fakes in a sandbox'
    it 'restores sinon on error'

  describe '#fakeweb', ->
    it 'mocks requests', (done) ->
      specHelper.fakeweb (web) =>
        web.get 'http://example.com', 200, 'BODY'

        http.get 'http://example.com', (res) ->
          res.on 'data', (chunk) ->
            assert.equal res.statusCode, 200
            assert.equal chunk, 'BODY'
            done()

    it 'mocks requests with headers', (done) ->
      specHelper.fakeweb (web) =>
        reqHeaders = {foo: 'FOO'}
        resHeaders = {bar: 'BAR'}

        web.get 'http://example.com', reqHeaders, 200, resHeaders, 'BODY'

        objUrl = url.parse('http://example.com')
        objUrl.headers = reqHeaders

        http.get objUrl, (res) ->
          res.on 'data', (chunk) ->
            assert.equal res.statusCode, 200
            assert.equal chunk, 'BODY'
            assert.equal res.headers.bar, 'BAR'
            done()

    it 'mocks redirects', (done) ->
      specHelper.fakeweb (web) =>
        web.redirect 'http://foo.com', 'http://bar.com'

        http.get 'http://foo.com', (res) ->
          res.on 'data', (chunk) ->
            assert.equal res.statusCode, 301
            assert.equal res.headers.location, 'http://bar.com'
            assert.equal chunk, ''
            done()

    it 'resets the created mocks', (done) ->
      specHelper.fakeweb (web) =>
        url = 'http://thisisareallyreallynonexistingdomain.com'

        web.get url, 200, 'BODY'
        web.reset()

        req = http.get url, (res) ->
        req.on 'error', (err) ->
          assert.equal err.code, 'ENOTFOUND'
          done()

  describe '#debug', ->
    it 'pretty prints objects', (done) ->
      obj =
        foo:
          bar: 'FOOBAR'
        baz: 'BAZ'

      expected = '{ foo: { bar: \u001b[32m\'FOOBAR\'\u001b[39m }, baz: \u001b[32m\'BAZ\'\u001b[39m }'

      sandbox = require('sinon').sandbox.create()
      sandbox.stub console, 'log', (text) ->
        sandbox.restore()
        assert.equal text, expected
        done()

      specHelper.debug(obj)

  describe '#fixture', ->
    it 'reads a fixture file'
