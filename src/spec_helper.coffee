sinon = require 'sinon'
chai = require 'chai'
sinonChai = require 'sinon-chai'
chai.use sinonChai
expect = chai.expect
proxyquire = require 'proxyquire'

url = require 'url'
nock = require 'nock'

util = require 'util'
path = require 'path'
fs = require 'fs'

sandbox = (fn) ->
  box = sinon.sandbox.create()
  if arguments.length == 0
    box
  else
    try
      fn(box)
      box.verify()
    finally
      box.restore()

fakeweb = (fn) ->
  splitUrl = (fullUrl) ->
    urlObj = url.parse(fullUrl)
    path = urlObj.pathname
    urlObj.pathname = ''
    baseUrl = url.format(urlObj)
    return [baseUrl, path]

  web =
    redirect: (fromUrl, toUrl) ->
      [fromBase, fromPath] = splitUrl(fromUrl)
      nock(fromUrl).get(fromPath).reply(301, '', {'Location': toUrl})

    mock: (verb, fullUrl, reqHeaders, reqBody, code, resHeaders, body) ->
      [base, path] = splitUrl(fullUrl)

      scope = nock(base)
      scope.matchHeader(k, v) for k, v of reqHeaders
      scope = if reqBody?
        scope.intercept(path, verb, reqBody)
      else
        scope.intercept(path, verb)
      scope.reply(code, body, resHeaders)

    # fullUrl, code, body
    # fullUrl, reqHeaders, code, resHeaders, body
    get: (fullUrl, reqHeaders, code, resHeaders, body) ->
      if typeof reqHeaders is 'number'
        body = code;
        resHeaders = {};
        code = reqHeaders;
        reqHeaders = {};
      @mock('GET', fullUrl, reqHeaders, null, code, resHeaders, body)

    reset: ->
      nock.cleanAll()

  if arguments.length == 0
    web
  else
    fn(web)

debug = (obj) ->
  options =
    showHidden: false
    depth: 4
    colors: true
    customInspect: true
  console.log util.inspect(obj, options)

fixture = (file) ->
  fs.readFileSync path.join(__dirname, 'fixtures', file), 'utf-8'

module.exports = {sinon, expect, proxyquire, sandbox, fakeweb, debug, fixture}
